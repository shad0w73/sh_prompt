#ifndef _CONFIG_H_
#define _CONFIG_H_

/* static */

#define SH_THEME_ESCAPE(seq)         "\x01\x1b[" seq "\x02"

#define SH_THEME_COLOR_RED           SH_THEME_ESCAPE("31m")
#define SH_THEME_COLOR_GREEN         SH_THEME_ESCAPE("32m")
#define SH_THEME_COLOR_BRIGHT_GREEN  SH_THEME_ESCAPE("92m")

#define SH_THEME_FONT_RESET          SH_THEME_ESCAPE("0m")
#define SH_THEME_FONT_BOLD           SH_THEME_ESCAPE("1m")


/* config */

static const char sh_theme_username_prefix[] = SH_THEME_COLOR_BRIGHT_GREEN;
static const char sh_theme_username_suffix[] = SH_THEME_FONT_RESET;

static const char sh_theme_hostname_prefix[] = "@";
static const char sh_theme_hostname_suffix[] = "";

static const char sh_theme_cwd_prefix[]      = " " SH_THEME_COLOR_GREEN;
static const char sh_theme_cwd_suffix[]      = SH_THEME_FONT_RESET;

static const char sh_theme_status_prefix[]   = " " SH_THEME_COLOR_RED "[" SH_THEME_FONT_BOLD;
static const char sh_theme_status_suffix[]   = SH_THEME_FONT_RESET SH_THEME_COLOR_RED "]" SH_THEME_FONT_RESET;

static const char sh_theme_git_prefix[]      = " (";
static const char sh_theme_git_suffix[]      = ")";

static const char sh_prompt_end[]            = "> ";
static const char sh_prompt_newline[]        = "⏎\n";

#endif /* _CONFIG_H_ */
