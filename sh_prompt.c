#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <limits.h>
#include <sys/uio.h>
#include <termios.h>
#include <unistd.h>

#include "config.h"

#define ESC '\x1b'

#define STRLEN(x) (sizeof(x)/sizeof(x[0])-1)

#define SIGSTR(sig) [sig] = #sig
#define SIG(name)   SIGSTR(SIG ## name)

enum variableString {
	newline     = 0,
	username    = 1,
	hostname    = 2,
	cwd         = 3,
	git         = 4,
	status      = 5,
	stringCount = 6
};

static const char *sigstr[32] = {
	[0] = "",    SIG(ABRT),   SIG(ALRM), SIG(BUS),
	SIG(CHLD),   SIG(CONT),   SIG(FPE),  SIG(HUP),
	SIG(ILL),    SIG(INT),    SIG(KILL), SIG(PIPE),
	SIG(POLL),   SIG(PROF),   SIG(PWR),  SIG(QUIT),
	SIG(SEGV),   SIG(STKFLT), SIG(STOP), SIG(SYS),
	SIG(TERM),   SIG(TRAP),   SIG(TSTP), SIG(TTIN),
	SIG(TTOU),   SIG(URG),    SIG(USR1), SIG(USR2),
	SIG(VTALRM), SIG(WINCH),  SIG(XCPU), SIG(XFSZ)
};

static bool cursor_in_first_column(int);
static void fishy_collapse_wd(char *);
static bool get_git_ref(const char *, struct iovec *);
static bool get_strings(struct iovec **, char *);
static bool need_newline(void);

bool
cursor_in_first_column(int fd)
{
	int i;
	char buf[BUFSIZ];
	ssize_t len;
	static const char out[4] = {ESC, '[', '6', 'n'};
	static const char start[2] = {ESC, '['};
	static const char expected[3] = {';', '1', 'R'};

	write(fd, out, sizeof(out));
	while (true) {
		len = read(fd, buf, sizeof(buf));

		for (i = 0; i < len - 5 && memcmp(&buf[i], start, sizeof(start)); i++);
		if (i >= len - 5)
			continue;

		for (i = i + 3; i < len - sizeof(expected) && buf[i] != ';'; i++);
		return !memcmp(&buf[i], expected, sizeof(expected));
	}
}

void
fishy_collapse_wd(char *i)
{
	char *o, *k;  // output and helper

	if (*i == '~')
		i++;

	k = o = i;
	while (*i) {
		*o++ = *i++;  // should always be '/'

		if (*i == '.')
			*o++ = *i++;
		if (*i)
			*o++ = *i++;
		for (k = i; *i && *i != '/'; i++);
	}
	memcpy(o, k, i-k+1);
}

bool
get_git_ref(const char *pwd, struct iovec *git)
{
	static char git_commit_ref[PATH_MAX];
	char *out = git_commit_ref;
	char *head = git_commit_ref;
	size_t out_len = sizeof(git_commit_ref);
	size_t len = strlen(pwd);
	static const char git_head_file[] = "/.git/HEAD";
	static const char git_head_ref[] = "ref: ";
	int fd;

	/* find repository and open HEAD file */
	memcpy(head, pwd, len);
	do {
		memcpy(&head[len], git_head_file, sizeof(git_head_file));
		if ((fd = open(head, O_RDONLY)) != -1)
			break;
		while (len-- && head[len] != '/');
	} while (len > 0);

	if (fd == -1)
		return false;

	/* get info */
	out_len = len = read(fd, out, out_len);
	close(fd);

	if (len < 8)
		return false;

	if (!strncmp(out, git_head_ref, strlen(git_head_ref))) {
		out[--len] = '\0';
		while (--len && out[len] != '/');
		out = &out[len+1];
		git->iov_len = out_len - len - 2;
	} else {
		out[8] = '\0';
		git->iov_len = 8;
	}
	git->iov_base = out;

	return true;
}

bool
get_strings(struct iovec **strings, char *rets)
{
	int ret;
	struct iovec home;

	home.iov_base               = getenv("HOME");
	strings[cwd]->iov_base      = getenv("PWD");
	strings[hostname]->iov_base = getenv("HOSTNAME");
	strings[username]->iov_base = getenv("USER");

	if (!(home.iov_base && strings[cwd]->iov_base && strings[hostname]->iov_base && strings[username]->iov_base))
		return false;

	/* check if newline needed */
	if (!need_newline())
		strings[newline]->iov_len = 0;

	/* username */
	strings[username]->iov_len = strlen(strings[username]->iov_base);

	/* hostname */
	strings[hostname]->iov_len = strlen(strings[hostname]->iov_base);

	/* git */
	if (!get_git_ref(strings[cwd]->iov_base, strings[git])) {
		(strings[git]-1)->iov_len = 0;
		(strings[git]+1)->iov_len = 0;
	}

	/* cwd */
	home.iov_len = strlen(home.iov_base);
	if (!strncmp(strings[cwd]->iov_base, home.iov_base, home.iov_len)) {
		strings[cwd]->iov_base = &((char*) strings[cwd]->iov_base)[home.iov_len-1];
		((char*) strings[cwd]->iov_base)[0] = '~';
	}
	fishy_collapse_wd(strings[cwd]->iov_base);
	strings[cwd]->iov_len = strlen(strings[cwd]->iov_base);

	/* return status */
	ret = atoi(rets);
	if (ret) {
		ret -= 128;
		if (ret > 0 && ret < 32)
			strings[status]->iov_base = (char*) sigstr[ret];
		else
			strings[status]->iov_base = rets;
		strings[status]->iov_len = strlen(strings[status]->iov_base);
	} else {
		(strings[status]-1)->iov_len = 0;
		(strings[status]+1)->iov_len = 0;
	}

	return true;
}

bool
need_newline(void)
{
	int tty;
	bool need_newline;
	struct termios old = {0};

	if ((tty = open("/dev/tty", O_RDWR | O_NOCTTY)) == -1)
		return true;

	tcgetattr(tty, &old);
	old.c_lflag &= ~ICANON;
	old.c_lflag &= ~ECHO;
	old.c_cc[VMIN] = 1;
	old.c_cc[VTIME] = 0;
	tcsetattr(tty, TCSANOW, &old);

	need_newline = !cursor_in_first_column(tty);

	old.c_lflag |= ICANON;
	old.c_lflag |= ECHO;
	tcsetattr(tty, TCSADRAIN, &old);

	close(tty);

	return need_newline;
}

int main(int argc, char *argv[])
{
	struct iovec *strings[stringCount];
	static struct iovec output[] = {
		{.iov_base = (void*) sh_prompt_newline,        .iov_len = STRLEN(sh_prompt_newline)},
		{.iov_base = (void*) sh_theme_username_prefix, .iov_len = STRLEN(sh_theme_username_prefix)},
		{.iov_base = NULL,                             .iov_len = 0},
		{.iov_base = (void*) sh_theme_username_suffix, .iov_len = STRLEN(sh_theme_username_suffix)},
		{.iov_base = (void*) sh_theme_hostname_prefix, .iov_len = STRLEN(sh_theme_hostname_prefix)},
		{.iov_base = NULL,                             .iov_len = 0},
		{.iov_base = (void*) sh_theme_hostname_suffix, .iov_len = STRLEN(sh_theme_hostname_suffix)},
		{.iov_base = (void*) sh_theme_cwd_prefix,      .iov_len = STRLEN(sh_theme_cwd_prefix)},
		{.iov_base = NULL,                             .iov_len = 0},
		{.iov_base = (void*) sh_theme_cwd_suffix,      .iov_len = STRLEN(sh_theme_cwd_suffix)},
		{.iov_base = (void*) sh_theme_git_prefix,      .iov_len = STRLEN(sh_theme_git_prefix)},
		{.iov_base = NULL,                             .iov_len = 0},
		{.iov_base = (void*) sh_theme_git_suffix,      .iov_len = STRLEN(sh_theme_git_suffix)},
		{.iov_base = (void*) sh_theme_status_prefix,   .iov_len = STRLEN(sh_theme_status_prefix)},
		{.iov_base = NULL,                             .iov_len = 0},
		{.iov_base = (void*) sh_theme_status_suffix,   .iov_len = STRLEN(sh_theme_status_suffix)},
		{.iov_base = (void*) sh_prompt_end,            .iov_len = STRLEN(sh_prompt_end)}
	};
	strings[newline]  = &output[0];
	strings[username] = &output[0*3+2];
	strings[hostname] = &output[1*3+2];
	strings[cwd]      = &output[2*3+2];
	strings[git]      = &output[3*3+2];
	strings[status]   = &output[4*3+2];

	if (argc != 2)
		return 1;

	if (!get_strings(strings, argv[1]))
		return 2;

	writev(STDOUT_FILENO, output, sizeof(output) / sizeof(*output));

	return 0;
}
