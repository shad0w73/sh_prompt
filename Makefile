.POSIX:
.SUFFIXES:
# Native
ARCH     = native
TUNE     = native
# Server1
#ARCH     = haswell
#TUNE     = haswell
# Server2
#ARCH     = nehalem
#TUNE     = nehalem

CC       = musl-clang -fuse-ld=lld
CFLAGS   = -march=$(ARCH) -mtune=$(TUNE) -O3 -std=c99 -Wall -Wpedantic -static
CPPFLAGS = -D_XOPEN_SOURCE
LDLIBS   =
LDFLAGS  = -Wl,-O1,--as-needed,-z,relro,-z,now

#CC       = clang -fuse-ld=lld
#CFLAGS   = -march=native -mtune=native -O2 -std=c99 -Wall -Wpedantic
#CPPFLAGS =
#LDLIBS   =
#LDFLAGS  = -Wl,-O1,--as-needed,-z,relro,-z,now

#CC       = gcc
#CFLAGS   = -O0 -ggdb
#CPPFLAGS =
#LDLIBS   =
#LDFLAGS  =

all: sh_prompt

sh_prompt: sh_prompt.c config.h
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -o $@ $< $(LDLIBS)
	strip -s -R .comment -R .gnu_version -g --strip-unneeded $@

clean:
	rm -f sh_prompt
